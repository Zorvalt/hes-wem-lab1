import com.google.gson.Gson;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class MyCrawler extends WebCrawler {
    public static final int BASE = 0;
    public static final int SPEC = 1;

    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|mp4|zip|gz))$");
    private final SolrClient solrClient = getSolrClient();
    private final Gson gson = new Gson();

    private String CORE;

    private final int crawlType;

    public MyCrawler(int crawlType) {
        this.crawlType = crawlType;

        switch (this.crawlType) {
            case MyCrawler.BASE:
                this.CORE = "wemlabo1";
                break;
            case MyCrawler.SPEC:
                this.CORE = "wemlabo2";
                break;
        }
    }

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILTERS.matcher(href).matches() && href.startsWith("https://www.1001cocktails.com/recettes");
    }

    @Override
    public void visit(Page page) {
        if (page.getParseData() instanceof HtmlParseData) {

            SolrInputDocument doc = null;

            switch (this.crawlType) {
                case MyCrawler.BASE:
                    doc = indexBase(page);
                    break;
                case MyCrawler.SPEC:
                    doc = indexSpec(page);
                    break;
            }

            try {
                solrClient.add(CORE, doc);
                solrClient.commit(CORE);
            } catch (SolrServerException | IOException e) {
                System.err.println("ERROR connecting core : " + CORE);
                System.err.println(e.getMessage());
            }
        }
    }

    public SolrInputDocument indexBase(Page page) {
        final SolrInputDocument doc = new SolrInputDocument();

        doc.addField("page", page);

        return doc;
    }

    public SolrInputDocument indexSpec(Page page) {
        HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();

        int docId = page.getWebURL().getDocid();
        String title = htmlParseData.getTitle();
        String text = htmlParseData.getText();
        String html = htmlParseData.getHtml();
        String url = page.getWebURL().getURL();
        Set<WebURL> links = htmlParseData.getOutgoingUrls();

        final SolrInputDocument doc = new SolrInputDocument();

        doc.addField("id", docId);
        doc.addField("title", title);
        doc.addField("url", url);
        doc.addField("text", text);
        doc.addField("html", html);
        doc.addField("links", links);

        Document jHtml = Jsoup.parse(html);

        Elements ingredients = jHtml.getElementsByClass("ingredient");
        List<String> ingredientList = new ArrayList<>();

        for (Element ingredient : ingredients) {
            ingredientList.add(ingredient.text());
        }

        Elements tags = jHtml.getElementsByClass("mrtn-tag");
        List<String> tagList = new ArrayList<>();

        for (Element tag : tags) {
            tagList.add(tag.text());
        }

        doc.addField("ingredients", gson.toJson(ingredientList.toString()));
        doc.addField("tags", gson.toJson(tagList.toString()));

        return doc;
    }

    public SolrDocumentList query(String field, String text) {
        try {
            final Map<String, String> queryParamMap = new HashMap<>();
            queryParamMap.put("q", field + ":" + text);
            queryParamMap.put("sort", "score DESC");

            MapSolrParams queryParams = new MapSolrParams(queryParamMap);

            final QueryResponse response = solrClient.query(CORE, queryParams);

            return response.getResults();
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private HttpSolrClient getSolrClient() {
        return new HttpSolrClient.Builder("http://localhost:8983/solr/").build();
    }
}