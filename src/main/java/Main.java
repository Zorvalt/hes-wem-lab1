import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        if (args.length < 2) {
            printUsage();
            System.exit(0);
        }

        switch (args[0]) {
            case "crawl":
                switch (args[1]) {
                    case "base":
                        crawl(MyCrawler.BASE, "wemlabo1");
                        break;

                    case "spec":
                        crawl(MyCrawler.SPEC, "wemlabo2");
                        break;

                    default:
                        System.err.println("Invalid command");
                        printUsage();
                        System.exit(1);
                }
                break;

            case "search":
                String[] query = args[1].split("=");

                if (query.length != 2) {
                    System.err.println("Invalid filter format");
                    printUsage();
                    System.exit(1);
                }
                search(query[0], query[1]);
                break;

            default:
                System.err.println("Invalid command");
                printUsage();
                System.exit(1);
        }
    }

    public static void printUsage() {
        System.out.println("Please use one of the following commands:\n" +
                "\t- crawl base\n" +
                "\t- crawl spec\n" +
                "\t- search filter=value\n" +
                "\n" +
                "Available filters are `ingredients`,  `tags`, `title`");
    }

    public static void crawl(int type, String core) {
        clearCore(core);

        String crawlStorageFolder = "./data/";
        int numberOfCrawlers = 8;

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setPolitenessDelay(1000);
        config.setMaxDepthOfCrawling(2);
        config.setMaxPagesToFetch(200);
        config.setIncludeBinaryContentInCrawling(false);
        config.setResumableCrawling(false);

        // Instantiate the controller for this crawl.
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);

        CrawlController controller;
        try {
            controller = new CrawlController(config, pageFetcher, robotstxtServer);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        // For each crawl, you need to add some seed urls. These are the first
        // URLs that are fetched and then the crawler starts following links
        // which are found in these pages
        controller.addSeed("https://www.1001cocktails.com/recettes/recette_mojito_354965.aspx");
        controller.addSeed("https://www.1001cocktails.com/recettes/recette_margarita_354941.aspx");
        controller.addSeed("https://www.1001cocktails.com/recettes/recette_caipirinha_354924.aspx");

        CrawlController.WebCrawlerFactory<MyCrawler> factory = () -> new MyCrawler(type);

        controller.start(factory, numberOfCrawlers);
    }

    public static void search(String field, String text) {

        MyCrawler crawler = new MyCrawler(MyCrawler.SPEC);
        SolrDocumentList results = crawler.query(field, text);

        if (results != null) {
            for (SolrDocument res : results) {
                System.out.println(res.getFieldValue("title"));
                System.out.println(" -> " + res.getFieldValue(field));
                System.out.println(" -> " + res.getFieldValue("url"));
            }
        }
    }

    public static void clearCore(String name) {
        HttpSolrClient client = new HttpSolrClient.Builder("http://localhost:8983/solr/").build();
        try {
            client.deleteByQuery(name, "*");
            client.commit(name);
        } catch (SolrServerException | IOException e) {
            System.err.println("Error clearing " + name + " documents");
        }
    }
}
